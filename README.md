﻿**PHP test project:**

There is a tree of start folder, it's subfolders, their subfolders, etc...

In each folder, subfolder, etc… there are same structured XML files stored. A sort of:

<book>

`    `<author>Isak Azimov</author>

`    `<name>End of spirit</name>

</book>

<book>

`    `<author>3</author>

`    `<name>Standard</name>

</book>

\1. Read XML parsed content into a data base table:
Mitko: OK ... 100%

1.1. PHP script should read XML files information and add it to **PostgreSQL** two database tables: “authors” and “books” (use 1:many and unique author’s ID as link between the tables). XML files content should be displayed as a result.
Mitko: OK ... 100%

1.2. If a record from specified file and subfolder already exists PHP script has to update the record and not to insert it as a new one. 
Mitko: OK ... 100%

\2. XML files should contain Cyrilic, Korean and Japanese symbols as well.
Mitko: OK ... 100%

\3. Create simple page with a search form (should search by author only from data base). Result should be printed right after search form. Search word should be populated to the input after submitting for better user experience. Data grid (result) should display the author and assigned books. Please use single sql query. Example *end* result:
Mitko: Half ... 50%

|Pavel Vejinov|Book 1|
| :- | :- |
|Pavel Vejinov|Book 2|
|Ivan Penev|<none> (no books found)|
|Blaga Dimitrova|Book Book 1|

Result design requirements: each row should slide from left to right one after another with some small animated delay.

Motion (animation) slide example:


|Pavel Vejinov|Book 1|
| :- | :- |



|Pavel Vejinov|Book 2|
| :- | :- |


|Ivan Penev|<none> (no books found)|
| :- | :- |


Etc…

\4. PHP script is supposed to be executed regularly as a Cron Job (Scheduled Task).
Mitko: OK ... 100%

**Optional advanced level addition:** Detect whether there is a large amount of data and speed up global Cron Job 3 (four) or more times. Optimize search result time which will affect when having large amount of data.
Mitko: OK ... 100%

**5. Requirements:** 

- Please use object oriented prorgamming; 
- Please do not use ready-made frameworks. Use your own PHP codes only;
- Please write short description of test project; 
- Please use HTML5 + CSS3 for design purposes;
- Please use only native JavaScript;
- Partial task solution is an option as well;
- Task additional questions are welcome;
- Test for unpredicted sutiations.

