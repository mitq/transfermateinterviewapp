<?php

use TransferMateInterviewApp\Repositories\AppRepository;
use TransferMateInterviewApp\Configs\AppConfig;
use TransferMateInterviewApp\Models\Process;

require_once realpath(__DIR__ . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");

$AppRepository = new AppRepository();
$Process = new Process();

$path = __DIR__ . AppConfig::$pathToXMLsDir;

if ($Process->isWorkNowProcess()) {
    die("Now work proccess. Wait next cron" . PHP_EOL);
}

try {
    $AppRepository->updateDatabaseDataByXMLBooksArray(
        $AppRepository->getAllUniqueBooksFromFilesOnDir(
            $path
        )
    );
} catch (\Exception $e) {
    $Process->stopWorkProcess();
    file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . AppConfig::$errorLogFile, $e->getMessage());
}

echo "done" . PHP_EOL;
