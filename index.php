<?php

use TransferMateInterviewApp\Configs\DatabaseConfig;
use TransferMateInterviewApp\Models\Database;
use TransferMateInterviewApp\Models\Book;

require_once realpath(__DIR__ . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");

$DatabaseConnection = (new Database(
    DatabaseConfig::$host,
    DatabaseConfig::$port,
    DatabaseConfig::$user,
    DatabaseConfig::$pass,
    DatabaseConfig::$name
))->getConnection();

$Book = new Book($DatabaseConnection);
$books = $Book->getAllBooksWithAuthors();

?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>TransferMate Interview App Title</title>
    <link rel="stylesheet" href="/css/main.css">
</head>

<body>
    <input id='myInput' onkeyup='searchTable()' placeholder="Search..." type='text'><br><br>
    <table id="books">
        <tr>
            <th>Name</th>
            <th>Author</th>
        </tr>

        <?php

        foreach ($books as $book) {
            echo "<tr>";
            echo "<td>{$book->title}</td>";
            echo "<td>{$book->name}</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>
<script src="/js/scripts.js"></script>
</html>