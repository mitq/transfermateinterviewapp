--
-- PostgreSQL database dump
--

-- Dumped from database version 10.18 (Ubuntu 10.18-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.8 (Ubuntu 12.8-1.pgdg18.04+1)

-- Started on 2021-09-13 13:21:29 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 197 (class 1259 OID 16388)
-- Name: authors; Type: TABLE; Schema: public; Owner: transfermate
--

CREATE TABLE public.authors (
    id integer NOT NULL,
    name character varying(200) NOT NULL
);


ALTER TABLE public.authors OWNER TO transfermate;

--
-- TOC entry 196 (class 1259 OID 16386)
-- Name: authors_id_seq; Type: SEQUENCE; Schema: public; Owner: transfermate
--

CREATE SEQUENCE public.authors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.authors_id_seq OWNER TO transfermate;

--
-- TOC entry 2933 (class 0 OID 0)
-- Dependencies: 196
-- Name: authors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: transfermate
--

ALTER SEQUENCE public.authors_id_seq OWNED BY public.authors.id;


--
-- TOC entry 199 (class 1259 OID 16398)
-- Name: books; Type: TABLE; Schema: public; Owner: transfermate
--

CREATE TABLE public.books (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    author_id integer
);


ALTER TABLE public.books OWNER TO transfermate;

--
-- TOC entry 198 (class 1259 OID 16396)
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: transfermate
--

CREATE SEQUENCE public.books_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.books_id_seq OWNER TO transfermate;

--
-- TOC entry 2934 (class 0 OID 0)
-- Dependencies: 198
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: transfermate
--

ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id;


--
-- TOC entry 2792 (class 2604 OID 16391)
-- Name: authors id; Type: DEFAULT; Schema: public; Owner: transfermate
--

ALTER TABLE ONLY public.authors ALTER COLUMN id SET DEFAULT nextval('public.authors_id_seq'::regclass);


--
-- TOC entry 2793 (class 2604 OID 16401)
-- Name: books id; Type: DEFAULT; Schema: public; Owner: transfermate
--

ALTER TABLE ONLY public.books ALTER COLUMN id SET DEFAULT nextval('public.books_id_seq'::regclass);


--
-- TOC entry 2925 (class 0 OID 16388)
-- Dependencies: 197
-- Data for Name: authors; Type: TABLE DATA; Schema: public; Owner: transfermate
--

COPY public.authors (id, name) FROM stdin;
2	Vanq Vanqqq
3	Isak Azimov
4	3
5	Anna
6	Anna2222
7	Anna222222222
8	Anna2222222222222
9	Anna2222222222222222242242
\.


--
-- TOC entry 2927 (class 0 OID 16398)
-- Dependencies: 199
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: transfermate
--

COPY public.books (id, title, author_id) FROM stdin;
3	Zaglawie2	2
4	End of spirit	3
5	Standard	4
6	End of spirit Две 2	2
7	Standard2	9
\.


--
-- TOC entry 2935 (class 0 OID 0)
-- Dependencies: 196
-- Name: authors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: transfermate
--

SELECT pg_catalog.setval('public.authors_id_seq', 9, true);


--
-- TOC entry 2936 (class 0 OID 0)
-- Dependencies: 198
-- Name: books_id_seq; Type: SEQUENCE SET; Schema: public; Owner: transfermate
--

SELECT pg_catalog.setval('public.books_id_seq', 7, true);


--
-- TOC entry 2795 (class 2606 OID 16395)
-- Name: authors authors_name_key; Type: CONSTRAINT; Schema: public; Owner: transfermate
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_name_key UNIQUE (name);


--
-- TOC entry 2797 (class 2606 OID 16393)
-- Name: authors authors_pkey; Type: CONSTRAINT; Schema: public; Owner: transfermate
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_pkey PRIMARY KEY (id);


--
-- TOC entry 2800 (class 2606 OID 16403)
-- Name: books books_pkey; Type: CONSTRAINT; Schema: public; Owner: transfermate
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- TOC entry 2798 (class 1259 OID 16409)
-- Name: idx_authors_name; Type: INDEX; Schema: public; Owner: transfermate
--

CREATE INDEX idx_authors_name ON public.authors USING btree (name);


--
-- TOC entry 2801 (class 1259 OID 16410)
-- Name: idx_books_title; Type: INDEX; Schema: public; Owner: transfermate
--

CREATE INDEX idx_books_title ON public.books USING btree (title);


--
-- TOC entry 2802 (class 2606 OID 16404)
-- Name: books fk_author; Type: FK CONSTRAINT; Schema: public; Owner: transfermate
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES public.authors(id) ON DELETE CASCADE;


-- Completed on 2021-09-13 13:21:29 EEST

--
-- PostgreSQL database dump complete
--

