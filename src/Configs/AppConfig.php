<?php

namespace TransferMateInterviewApp\Configs;

class AppConfig {
    static $pathToXMLsDir = DIRECTORY_SEPARATOR . 'dirs' . DIRECTORY_SEPARATOR . '*.xml';
    static $isHaveXMLFilesRootElement = false;
    static $rootElementNameXML = 'books';
    static $isActiveProccessProtect = true;
    static $fileForCheckProcess = 'file_for_check_is_active_proccess_for_cron.dont_delete_it';
    static $errorLogFile = 'trow_errors';
}
