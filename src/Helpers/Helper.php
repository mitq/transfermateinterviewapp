<?php 

namespace TransferMateInterviewApp\Helpers;

class Helper{

    /*
    * Use glob to do recursive search
    * get from - https://stackoverflow.com/a/17161106  
    * Does not support flag GLOB_BRACE
    */
    
    public static function rglob($pattern, $flags = 0) {
        $files = glob($pattern, $flags); 
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, self::rglob($dir.'/'.basename($pattern), $flags));
        }
        return $files;
}

}