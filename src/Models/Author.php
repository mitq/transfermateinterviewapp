<?php

namespace TransferMateInterviewApp\Models;

class Author extends BaseDatabaseModel
{

    private $databaseName = 'authors';

    public function __construct($DBConnect)
    {
        parent::__construct($DBConnect);

        $this->setTable(
            $this->databaseName
        );
    }

    public function getByName($name)
    {
        return $this->getByNameField($name, 'name');
    }

    public function addNewRecordInDB($name)
    {
        $addNewRecordInDBSQL = 'INSERT INTO ' . $this->getTable() . '(name) VALUES(:name)';
        $name = trim($name);

        $pdoSql = $this->getDBConnect()->prepare($addNewRecordInDBSQL);
        $pdoSql->bindParam(':name', $name, \PDO::PARAM_STR);
        $status = $pdoSql->execute();

        if (!$status) {
            throw new \Exception(time() . " -Error in create new record for table " . $this->getTable() . PHP_EOL, 352);
        }

        $pdoSql->fetchAll(\PDO::FETCH_OBJ);

        return $this->getByID($this->getDBConnect()->lastInsertId());
    }

}
