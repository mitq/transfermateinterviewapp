<?php

namespace TransferMateInterviewApp\Models;

class BaseDatabaseModel
{
    private $DBConnect;
    private $table;

    public function __construct($DBConnect)
    {
        $this->setDBConnect($DBConnect);
    }

    public function getByNameField($value, $nameField)
    {
        $value = trim($value);
        $nameFieldBind = ':' . $nameField;
        $getByNameSQL = 'SELECT * FROM ' . $this->getTable() . ' WHERE ' . $nameField . '  = :' . $nameField;
        $pdoSql = $this->getDBConnect()->prepare($getByNameSQL);
        $pdoSql->bindParam($nameFieldBind, $value, \PDO::PARAM_STR);
        $status = $pdoSql->execute();

        if (!$status) {
            throw new \Exception(time() . " - Error in get by name for table " . $this->getTable() . PHP_EOL, 351);
        }

        $result = $pdoSql->fetchAll(\PDO::FETCH_OBJ);

        return empty($result) ? null : reset($result);
    }

    public function getByID($id)
    {
        $id = intval($id);

        $getByIDSQL = 'SELECT * FROM ' . $this->getTable() . ' WHERE id  = :id';
        $pdoSql = $this->getDBConnect()->prepare($getByIDSQL);
        $pdoSql->bindParam(':id', $id, \PDO::PARAM_INT);
        $status = $pdoSql->execute();

        if (!$status) {
            throw new \Exception(time() . " - Error in get by ID for table " . $this->getTable() . PHP_EOL , 353);
        }

        $result = $pdoSql->fetchAll(\PDO::FETCH_OBJ);

        return empty($result) ? null : reset($result);
    }

    /**
     * Get the value of DBConnect
     */
    public function getDBConnect()
    {
        return $this->DBConnect;
    }

    /**
     * Set the value of DBConnect
     *
     * @return  self
     */
    public function setDBConnect($DBConnect)
    {
        $this->DBConnect = $DBConnect;

        return $this;
    }

    /**
     * Get the value of table
     */
    public function getTable()
    {
        return $this->table;
    }



    /**
     * Set the value of table
     *
     * @return  self
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }
}
