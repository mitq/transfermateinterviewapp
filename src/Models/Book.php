<?php

namespace TransferMateInterviewApp\Models;

class Book extends BaseDatabaseModel
{

    private $databaseName = 'books';

    public function __construct($DBConnect)
    {
        parent::__construct($DBConnect);

        $this->setTable(
            $this->databaseName
        );

    }

    public function getByName($name)
    {
        return $this->getByNameField($name, 'title');
    }

    public function addNewRecordInDB($title, $author_id)
    {
        $addNewRecordInDBSQL = 'INSERT INTO '. $this->getTable() .'(title, author_id) VALUES(:title, :author_id)';

        $title = trim($title);
        $author_id = intval(trim($author_id));

        $pdoSql = $this->getDBConnect()->prepare($addNewRecordInDBSQL);
        $pdoSql->bindParam(':title', $title, \PDO::PARAM_STR);
        $pdoSql->bindParam(':author_id', $author_id, \PDO::PARAM_INT);
        $status = $pdoSql->execute();

        if(!$status){
            throw new \Exception(time() . " -Error in create new record for table " . $this->getTable(). PHP_EOL, 352);
        }

        $pdoSql->fetchAll(\PDO::FETCH_OBJ);

        return $this->getByID($this->getDBConnect()->lastInsertId());
        
    }

    public function updateAuthorIDByBookID(int $bookDataId, int $authorDataId){

        $updateNewRecordInDBSQL = 'UPDATE ' . $this->getTable() . ' SET author_id = :author_id WHERE id = :id';

        $pdoSql = $this->getDBConnect()->prepare($updateNewRecordInDBSQL);
        $pdoSql->bindParam(':author_id', $authorDataId, \PDO::PARAM_INT);
        $pdoSql->bindParam(':id', $bookDataId, \PDO::PARAM_INT);
        $status = $pdoSql->execute();

        if (!$status) {
            throw new \Exception(time() . " -Error in create new record for table " . $this->getTable() . PHP_EOL, 352);
        }

        $pdoSql->fetchAll(\PDO::FETCH_OBJ);

        return $this->getByID($bookDataId);
    }

    public function getAllBooksWithAuthors(){
        $allWithAuthorsJoinSql = 'SELECT * FROM ' . $this->getTable() . ' INNER JOIN authors ON '. $this->getTable() .'.author_id = authors.id;';

        $pdoSql = $this->getDBConnect()->prepare($allWithAuthorsJoinSql);
        $status = $pdoSql->execute();

        if (!$status) {
            throw new \Exception(time() . " -Error in get all books " . $this->getTable() . PHP_EOL, 352);
        }

        return  $pdoSql->fetchAll(\PDO::FETCH_OBJ);
    }

    
    
}
