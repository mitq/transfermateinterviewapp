<?php

namespace TransferMateInterviewApp\Models;

class Database
{

    private $connection;

    public function __construct($host, $port, $user, $pass, $name)
    {

        try {
            $dsn = "pgsql:host={$host};port={$port};dbname=$name;";
            $this->connection = new \PDO($dsn, $user, $pass);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getConnection()
    {
        return $this->connection;
    }
}
