<?php

namespace TransferMateInterviewApp\Models;

class File {

    private $file;
    private $content;

    /*
    * Check content is empty
    */

    public function checkIsHaveContentOrThrowException()
    {

        $this->addPrivateContent();

        if(empty($this->getContent())){
            throw new \Exception(time() . " -The file is with empty content for file" . PHP_EOL, 1);
        }

        return $this;
    }

    private function addPrivateContent()
    {
        $this->setContent(
            file_get_contents(
                $this->getFile()
            )
        );
    }

    /**
     * Get the value of file
     */ 
    private function getFile()
    {
        return $this->file;
    }

    /**
     * Set the value of file
     *
     * @return  self
     */ 
    public function setFile($file = '')
    {

        if(empty($file)){
            throw new \Exception(time() . " -Need set currect file" . PHP_EOL, 1);
        }

        if(!file_exists($file)){
            throw new \Exception(time() . " -File is not exists" . PHP_EOL, 1);
        }

        $this->file = $file;

        return $this;
    }

    /**
     * Get the value of content
     */ 
    public function getContent()
    {
        if(empty($this->content)){
            $this->addPrivateContent();
        }

        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */ 
    private function setContent($content)
    {

        $this->content = $content;

        return $this;
    }
}
