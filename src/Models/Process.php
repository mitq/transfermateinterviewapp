<?php

namespace TransferMateInterviewApp\Models;

use TransferMateInterviewApp\Configs\AppConfig;

class Process {

    private $fileForCheckProcess;

    public function __construct()
    {
        $this->setFileForCheckProcess(AppConfig::$fileForCheckProcess);
    }
    public function startWorkNowProcess()
    {
        file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . $this->getFileForCheckProcess(), 'working...');
    }

    public function isWorkNowProcess()
    {
        return file_exists(__DIR__ . DIRECTORY_SEPARATOR . $this->getFileForCheckProcess());
    }

    public function stopWorkProcess()
    {
        return unlink(__DIR__ . DIRECTORY_SEPARATOR . $this->getFileForCheckProcess());
    }

    /**
     * Get the value of fileForCheckProcess
     */ 
    private function getFileForCheckProcess()
    {
        return $this->fileForCheckProcess;
    }

    /**
     * Set the value of fileForCheckProcess
     *
     * @return  self
     */ 
    private function setFileForCheckProcess($fileForCheckProcess)
    {
        $this->fileForCheckProcess = $fileForCheckProcess;

        return $this;
    }

}
