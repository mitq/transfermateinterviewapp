<?php

namespace TransferMateInterviewApp\Repositories;

use TransferMateInterviewApp\Configs\AppConfig;
use TransferMateInterviewApp\Configs\DatabaseConfig;
use TransferMateInterviewApp\Models\File;
use TransferMateInterviewApp\Models\Database;
use TransferMateInterviewApp\Models\Book;
use TransferMateInterviewApp\Models\Author;
use TransferMateInterviewApp\Models\Process;
use TransferMateInterviewApp\Helpers\Helper;

class AppRepository {
    
    public function getAllUniqueBooksFromFilesOnDir($path)
    {
        
        $isHaveXMLFilesRootElement = AppConfig::$isHaveXMLFilesRootElement;
        $rootElementNameXML = AppConfig::$rootElementNameXML;

        $File = new File();

        $filenames  = Helper::rglob($path);
        $xmlBooks = [];

        foreach ($filenames as $filename) {
            $xmlRawContent = $File->setFile($filename)->checkIsHaveContentOrThrowException()->getContent();

            if (!$isHaveXMLFilesRootElement) {
                $xmlRawContent = "<{$rootElementNameXML}>{$xmlRawContent}</{$rootElementNameXML}>";
            }

            $booksFromOneXML = new \SimpleXMLElement($xmlRawContent);

            foreach ($booksFromOneXML->book as $book) {
                $xmlBooks[] = ((array) $book);
            }
        }

        return array_unique($xmlBooks, SORT_REGULAR);
        
    }

    public function updateDatabaseDataByXMLBooksArray(array $xmlBooks)
    {

        $Process = new Process();
        $Process->startWorkNowProcess();

        $DatabaseConnection = (new Database(
            DatabaseConfig::$host,
            DatabaseConfig::$port,
            DatabaseConfig::$user,
            DatabaseConfig::$pass,
            DatabaseConfig::$name
        ))->getConnection();

        foreach ($xmlBooks as $book) {
            $author = new Author($DatabaseConnection);
            $authorData = $author->getByName($book['author']);
            if(is_null($authorData)){
                $authorData = $author->addNewRecordInDB($book['author']);
            }
        
            $BookModel = new Book($DatabaseConnection);
            $bookData = $BookModel->getByName($book['name']);

            if(!is_null($bookData) && ($bookData->author_id != $authorData->id)){
                $bookData = $BookModel->updateAuthorIDByBookID($bookData->id, $authorData->id);
            }

            if(is_null($bookData)){
                $bookData = $BookModel->addNewRecordInDB($book['name'], $authorData->id);
            }
        }

        $Process->stopWorkProcess();
    }


}